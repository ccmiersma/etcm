# etcm

This script manages configuration files that exist outside of /etc by mirroring them into a /etc, so that all configuration is in one place that can be backed up or managed by a tool like etckeeper.

Further docs can be found [here](https://ccmiersma.gitlab.io/etcm). The documentation is automatically built from the docs folder using [Mkdocs](http://www.mkdocs.org/).

The man pages are also automatically generated using pandoc.

