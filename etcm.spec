%define author Christopher Miersma

Name:		etcm
Version:        0.3.1
Release:        1.local%{?dist}

Summary:	Manage config files outside /etc
Group:		Utilities
License:	MIT
URL:		https://gitlab.com/ccmiersma/%{name}/
Source0:	%{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  pandoc
BuildRequires:  ruby
BuildRequires: 	rubygems
Requires:       rsync

%description
This script manages configuration files that exist outside of /etc by mirroring them into a /etc, so that all configuration is in one place that can be backed up or managed by a tool like etckeeper.

%{!?local_prefix:%define local_prefix %(echo %{release} | cut -f2 -d. | egrep -v "(^el[[:digit:]]|^ol[[:digit:]]|^fc[[:digit:]])" )}


%if "%{local_prefix}" != "" && "%{local_prefix}" != "git"  
  %if "%{local_prefix}" == "opt"  
    %define _prefix /opt
  %else
    %define _prefix /opt/%{local_prefix}
  %endif
  %define _sysconfdir /etc/%{_prefix}
  %define _localstatedir /var/%{_prefix}
  %define _datadir %{_prefix}/share
  %define _docdir %{_datadir}/doc
  %define _mandir %{_datadir}/man
  %define _bindir %{_prefix}/bin
  %define _sbindir %{_prefix}/sbin
  %define _libdir %{_prefix}/lib
  %define _libexecdir %{_prefix}/libexec
  %define _includedir %{_prefix}/include
%endif


%prep
%setup


%build

./configure --prefix=%_prefix --sysconfdir=%_sysconfdir --localstatedir=%_localstatedir --destdir=%buildroot
make

%install


make install

#Manually defined files and dirs that need special designation.
#This will end up in the files section.
cat > %{name}-defined-files-list << EOF
%config(noreplace) %_sysconfdir/%{name}/
%docdir %{_mandir}
%docdir %{_docdir}
EOF
#Convoluted stuff to combine the manual list above with any new files we find, into a correct list with no duplicates
find ${RPM_BUILD_ROOT} -type f -o -type l | sed -e "s#${RPM_BUILD_ROOT}##g"|sed -e "s#\(.*\)#\"\1\"#" > %{name}-all-files-list
cat %{name}-defined-files-list | cut -f2 -d' ' | sed -e "s#\(.*\)#\"\1\"#" | sort > %{name}-defined-files-list.tmp
cat %{name}-all-files-list | sort > %{name}-auto-files-list.tmp
diff -e %{name}-defined-files-list.tmp %{name}-auto-files-list.tmp | grep "^\"" > %{name}-auto-files-list
cat %{name}-defined-files-list %{name}-auto-files-list > %{name}-files-list

%clean
%__rm -rf ${RPM_BUILD_ROOT}

%files -f %{name}-files-list
%defattr(-,root,root, -)


# The post and postun update the man page database
%post

#mandb
#systemctl daemon-reload

%postun

#mandb
#systemctl daemon-reload

%changelog
* Fri Dec 08 2017 Christopher Miersma <ccmiersma@gmail.com> 0.3.1-1.local
- Adjusted CI config. (ccmiersma@gmail.com)
- Fixed out put of info; added basic test. (ccmiersma@gmail.com)
- Adjusted CI config again. (ccmiersma@gmail.com)
- Adjusted CI config. (ccmiersma@gmail.com)

* Fri Dec 08 2017 Christopher Miersma <ccmiersma@gmail.com> 0.3.0-1.local
- Adjusted CI config. (ccmiersma@gmail.com)
- Added build root to configure (ccmiersma@gmail.com)
- New scheme for building from templates. (ccmiersma@gmail.com)
- Added extra commands. (ccmiersma@gmail.com)
- Fixed specfile bug. (ccmiersma@gmail.com)
- Extended README, added man page code. (ccmiersma@gmail.com)
- Automatic location based on dist tag (ccmiersma@gmail.com)
- Finished basic man pages and linked documentation. (ccmiersma@gmail.com)
- Add license (ccmiersma@gmail.com)
- Adjusted CI to remove before_script (ccmiersma@gmail.com)
- Adjusted stages. (ccmiersma@gmail.com)
- Marked pages as needing docker. (ccmiersma@gmail.com)
- Moved README back and added pages to CI. (ccmiersma@gmail.com)
- Added tags to CI (ccmiersma@gmail.com)
- Added placeholder CI (ccmiersma@gmail.com)
- Changed README to link (ccmiersma@gmail.com)
- Extended documentation. (ccmiersma@gmail.com)
- Basic documentation started. (ccmiersma@gmail.com)

* Mon Nov 27 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.2-1.local
- Adjusted etcm_ls to calculate rsync list (ccmiersma@gmail.com)
- Added etcm_info function (ccmiersma@gmail.com)
- Added etcm_ls function (ccmiersma@gmail.com)

* Mon Nov 27 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.1-2.local
- Initial working release

* Mon Nov 27 2017 Christopher Miersma <ccmiersma@gmail.com> 0.1.1-1.local
- new package built with tito




