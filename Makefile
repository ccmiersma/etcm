all: build 

build: build-config.rb src/etcm.lib.sh.erb src/etcm.conf.erb src/etcm.erb docs/man.etcm.1.md
	mkdir build 
	erb -r ./build-config.rb src/etcm.lib.sh.erb > build/etcm.lib.sh
	erb -r ./build-config.rb src/etcm.conf.erb > build/etcm.conf
	erb -r ./build-config.rb src/etcm.erb > build/etcm
	pandoc -s -t man docs/man.etcm.1.md | gzip > build/etcm.1.gz

install:	build/etcm.lib.sh build/etcm.conf build/etcm README.md
	install -d -m 755 \
		././build/test//share/doc/etcm/ \
		././build/test//share/etcm/  \
		././build/test//bin \
		././build/test//etcm/\
		././build/test//share/man/man1/
	install -m 644 build/etcm.lib.sh ././build/test//share/etcm/ 
	install -m 644 build/etcm.conf src/*-files ././build/test//etcm/
	install -m 644 README.md LICENSE ././build/test//share/doc/etcm/
	install -m 755 build/etcm ././build/test//bin/
	install -m 644 build/etcm.1.gz ././build/test//share/man/man1/

clean:
	rm -rf build
