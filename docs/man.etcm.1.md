---
title: etcm
author: Christopher Miersma
date: November 2017
section: 1
header: etcm - Documentation 
footer: etcm 0.2.x
---

# NAME

etcm - manage files outside /etc by mirroring them into /etc

# SYNOPSIS

**etcm command [file/or/dir/path]**

# DESCRIPTION

This script manages configuration files that exist outside of /etc by mirroring them into a /etc, so that all configuration is in one place that can be backed up or managed by a tool like etckeeper.

If a command takes an argument, the argument can be a full or relative path to a file or directory. If no argument is given, it automatically assumes the current directory.

# COMMANDS

**add** [argument] This adds a file to the manual list of files and syncs it immediately. It is equivalent to **track** and **ci**.

**ci** [argument] This immediately syncs a file whether it is in the manual list or not. This can be best be used with files marked as *config* by RPMs.

**co** [argument] This immediately restores a file whether it is in the manual list or not.

**diff** [argument]

**ls** List all the files that would be copied in a sync.

**info** Lists all the files and stats from actually running rsync.

**rm** [argument] Delete a file from the mirror-root and **untrack** it.

**sync** This syncs all the files. It automatically generates a list of files form RPM config files as well as the manual list of files.

**track** [argument] Add a file or directory to the manual sync list.

**untrack** [argument] Remove a file from the manual sync list.

