# etcm

This script manages configuration files that exist outside of /etc by mirroring them into a /etc, so that all configuration is in one place that can be backed up or managed by a tool like etckeeper.

